from typing import Dict, List, Tuple, Set
from colors import Colors
from player import Player
from collections import namedtuple
import json
import numpy as np


# It's a UI class composed entirely of class methods. All of the I/O stuff gets it's own file cause
# it's ugly to look at and plagues the main file.
class ConsoleUI:
    ai_names: Dict[int, str] = {7: "GavBot", 8: "August", 9: "Ollie", 10: "A-man"}

    # Tokens to be placed on tiles that represent the player
    tokens: Dict[int, str] = {0: "   ", 1: " 🦁", 2: " 🦊", 3: " 🐻", 4: " 🐹", 5: " 🐤", 6: " 👽", 7: " 🤖", 8: " ⌨️",
                              9: " 📟", 10: " 💾", 11: "", 12: " 🔵"}

    # Uncomment me if you're using windows and emojis are screwed up
    #tokens: Dict[int, str] = {0: "   ", 1: "  L", 2: "  C", 3: "  B", 4: "  M", 5: "  D", 6: "  Z", 7: "  G", 8: "  A️",
                              #9: "  T", 10: "  F", 11: "", 12: "  X"}

    # Suits for the cards
    suits: Dict[int, str] = {
        0: Colors.BLUE + "W" + Colors.GREEN + "I" + Colors.YELLOW + "L" + Colors.RED + "D" + Colors.ENDL,
        1: Colors.BOLD + Colors.BLUE + "♠" + Colors.ENDL,
        2: Colors.BOLD + Colors.GREEN + "♣" + Colors.ENDL,
        3: Colors.BOLD + Colors.YELLOW + "♦" + Colors.ENDL,
        4: Colors.BOLD + Colors.RED + "♥" + Colors.ENDL}

    # Value associated with a card
    values: Dict[int, str] = {0: " ", 1: "A", 2: "2", 3: "3", 4: "4", 5: "5", 6: "6", 7: "7", 8:
        "8", 9: "9", 10: "T", 11: "J", 12: "Q", 13: "K"}

    @classmethod
    # Pretty prints the board!
    def pretty_print(cls, b: np.array, d: Dict):
        # Prints the column labels
        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        print("        A         B        C        D        E       F        G        H        I        J")
        for yy in range(10):
            # Prints the row label
            print(str(yy) + "   ", end="")
            for xx in range(10):
                # If the spot is unclaimed, then the card is shown at that spot
                card_c = d[(xx, yy)]
                # Prints a string representation of the numerical card. The tens place is the value
                # (// is integer division) and the ones place is the suit. The space next to it is then
                # Filled in by the token covering that spot.
                print("[ " + cls.values[card_c // 10] + cls.suits[card_c % 10] + cls.tokens[b[xx][yy]] + " ]", end="")

            print()

    @classmethod
    # Pretty prints the board!
    def pretty_print_lm(cls, b: np.array, d: Dict, lm: Tuple[int, int]):
        # Prints the column labels
        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        print("        A         B        C        D        E       F        G        H        I        J")
        for yy in range(10):
            # Prints the row label
            print(str(yy) + "   ", end="")
            for xx in range(10):
                # If the spot is unclaimed, then the card is shown at that spot
                card_c = d[(xx, yy)]
                # Prints a string representation of the numerical card. The tens place is the value
                # (// is integer division) and the ones place is the suit. The space next to it is then
                # Filled in by the token covering that spot.
                if (xx, yy) != lm:
                    print("[ " + cls.values[card_c // 10] + cls.suits[card_c % 10] + cls.tokens[b[xx][yy]] + " ]",
                          end="")
                else:
                    print("[ " + Colors.HIGHLIGHT + cls.values[card_c // 10] + cls.suits[card_c % 10] + cls.tokens[
                        b[xx][yy]] + " ]",
                          end="")

            print()

    @classmethod
    # Pretty prints the board with blue spaces on any items in s (helps see legal moves for humans)
    def pretty_print_legals(cls, b: np.array, d: Dict, s: Set):
        # Prints the column labels
        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        print("        A         B        C        D        E       F        G        H        I        J")
        loc_set: Set = set()
        for tt, cc in s:
            loc_set.add((tt[0], tt[1]))
        for yy in range(10):
            # Prints the row label
            print(str(yy) + "   ", end="")
            for xx in range(10):
                card_c = d[(xx, yy)]
                tt: int = b[xx][yy] if (xx, yy) not in loc_set else 12
                # Prints a string representation of the numerical card. The tens place is the value
                # (// is integer division) and the ones place is the suit. The space next to it is then
                # Filled in by the token covering that spot.
                print("[ " + cls.values[card_c // 10] + cls.suits[card_c % 10] + cls.tokens[tt] + " ]", end="")

            print()

    # Prints out a player's hand
    @classmethod
    def pretty_print_hand(cls, n: str, h: List):
        print(n + "'s hand: ")
        for i in range(len(h)):
            print("  " + str(i + 1) + "  ", end="")
        print()
        for card_c in h:
            print("[" + cls.values[card_c // 10] + cls.suits[card_c % 10] + " ]", end="")
        print()

    @classmethod
    def show_msg(cls, msg: str):
        print(msg)

    @classmethod
    def show_good(cls, msg: str):
        print(Colors.GREEN + msg + Colors.ENDL)

    @classmethod
    def show_warn(cls, msg: str):
        print(Colors.RED + msg + Colors.ENDL)

    @classmethod
    def show_debug(cls, msg: str):
        print(Colors.YELLOW + msg + Colors.ENDL)

    @classmethod
    def get_number(cls, prompt: str) -> int:
        ans: int = None
        while ans is None:
            try:
                ans: int = int(input(prompt))
            except ValueError:
                cls.show_warn("Enter a number only!")
        return ans

    @classmethod
    def get_card(cls, h: List[int]) -> int:
        selected: int
        valid: bool = False
        while not valid:
            selected = cls.get_number("Choose a card to play")
            selected -= 1
            if 0 <= selected < len(h):
                valid = True
        return selected

    @classmethod
    def get_move(cls, legal_moves: Set[Tuple[int, int]]) -> Tuple[int, int] or None:
        col: int = -1
        row: int = -1
        valid: bool = False
        legal_locs: Set = set()
        for tt, cc in legal_moves:
            legal_locs.add((tt[0], tt[1]))
        while not valid:
            choice: str = input("Choose a space to claim, or press enter to cancel")
            choice = choice.lower()
            if len(choice) != 2:
                return None
            str_col: str = choice[0]
            str_row: str = choice[1]
            if str_col.isdigit():
                t = str_row
                str_row = str_col
                str_col = t
            col = ord(str_col) - 97
            row = int(str_row)
            if 0 <= row <= 9 and 0 <= col <= 9:
                if (col, row) in legal_locs:
                    valid = True
                else:
                    cls.show_warn("You can't place that card there!")
            else:
                cls.show_warn("That is an invalid space!")
        return col, row

    @classmethod
    def get_players(cls) -> List:
        done: bool = False
        player_list: List = []
        load: int = cls.get_number("Enter 0 to load from Players.json, or any other number otherwise")
        if load == 0:
            with open('Players.json') as f:
                players_load = json.load(f)
            for name, player in players_load['Players'].items():
                player_list.append(Player(name, player.get('token'), player.get('is_ai')))
            return player_list
        while len(player_list) < 4 and not done:
            choice: int = cls.get_number("1. Register a human player\n2. Register a MCTS opponent\n"
                                         "3. Register a HAI opponent\n4. Register a HATS opponent\n5. Start the game\n")
            while 1 > choice or choice > 5:
                choice = cls.get_number("Invalid choice, pick the number associated with your choice!\n"
                                        "1. Register a human player\n2. Register a MCTS opponent\n"
                                        "3. Register a HAI opponent\n4. Register a HATS opponent\n5. Start the game\n")
            if choice == 1:
                player_name: str = input("Give us a name to register\n")
                token_choice: int = None
                while token_choice is None:
                    print("Select a token to represent you")
                    for i in range(1, 7):
                        print(i, ": ", cls.tokens[i])
                    token_choice = cls.get_number("")
                    if token_choice > 6:
                        token_choice = None
                    for player in player_list:
                        if token_choice == player.token:
                            token_choice = None
                            cls.show_warn("Somebody is already using that token")
                player_list.append(Player(player_name, token_choice, 0))
            elif int(choice) == 2:
                token_choice: int = 7
                for player in player_list:
                    if token_choice == player.token:
                        token_choice += 1
                player_list.append(Player(cls.ai_names[token_choice], token_choice, 1))
                print("Registered MCTS player " + cls.ai_names[token_choice])
            elif int(choice) == 3:
                token_choice: int = 7
                for player in player_list:
                    if token_choice == player.token:
                        token_choice += 1
                player_list.append(Player(cls.ai_names[token_choice], token_choice, 2))
                print("Registered HAI player " + cls.ai_names[token_choice])
            elif int(choice) == 4:
                token_choice: int = 7
                for player in player_list:
                    if token_choice == player.token:
                        token_choice += 1
                player_list.append(Player(cls.ai_names[token_choice], token_choice, 3))
                print("Registered HATS player " + cls.ai_names[token_choice])
            elif int(choice) == 5:
                if len(player_list) > 1:
                    done = True
                else:
                    cls.show_warn("You need at least two players to play the game!")
        return player_list
