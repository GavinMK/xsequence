import numpy as np


# Represents a player of the game
class Player:

    # Slots basically tell python not to use a dict to store our fields. Accessing attributes
    # Is faster this way and uses significantly less memory, which is important since new players
    # will be created all the time by the AI.
    __slots__ = 'name', 'token', 'hand', 'is_ai'

    def __init__(self, n: str, t: int, a: int, h: np.array = None):
        self.name = n
        self.token = t
        self.hand = h
        self.is_ai = a

    def create_hand(self, s: int):
        self.hand = np.zeros(s)
