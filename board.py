import numpy as np
import json
from ui import ConsoleUI
import copy
from player import Player
from typing import List, Dict, Tuple, Set

with open('Cards.json') as f:
    Cards = json.load(f)

# A dictionary that represents card placement on the board. The key is a 2 character representation of a
# card, and the value is a tuple containing another tuple that is the locations that card appears in.
# Useful for easily figuring out where cards in hand can be placed.
card_to_cords: Dict[int, List[Tuple]] = {}

# A dictionary that also represents card placement. The key for this one however is a coordinate, and the
# value is the card at that coordinate. Useful for identifying the card at a certain coordinate.
cords_to_card: Dict[Tuple, int] = {}

# A for loop that populates the dictionaries
for name, cardi in Cards['Cards'].items():
    board_positions = []
    locations = cardi.get('BoardLocations')
    for pos in locations:
        board_positions.append((pos.get('x'), pos.get('y')))
        cords_to_card[(pos.get('x'), pos.get('y'))] = cardi.get('Code')
    card_to_cords[cardi.get('Code')] = board_positions


class Board:

    # An array of bytes that represents token placement on the board. This array is separate from the
    # board itself because the AI needs to manipulate copies of the board. Token placement is the only
    # changing aspect of the board, so it saves time and spice to only copy this array.
    tboard: np.array

    deck: np.array

    last_move: Tuple[Tuple[int, int], int]

    last_player: int

    players: List[Player]

    current: int

    turn_count: int

    cords_to_card: Dict

    card_to_cords: Dict

    victor: int

    draw_count: int

    def __init__(self, clean: bool):

        self.cords_to_card = cords_to_card

        self.card_to_cords = card_to_cords

        if clean:
            self.tboard = np.zeros((10, 10), dtype=np.int8)

            self.turn_count = 0

            self.last_move = None

            self.last_player = None

            self.make_new_deck()

            self.players = None

            self.current = 0

            self.victor = None

            self.draw_count = 0

            # Initializes wildcards to their respective token
            for wildcard in self.card_to_cords[0]:
                self.tboard[wildcard[0]][wildcard[1]] = 11

    # Creates and returns a randomly sorted standard sequence deck.
    def make_new_deck(self) -> np.array:

        # An array of unsigned bytes that represents cards in the deck. This array also needs to be
        # copied many times for the AI and so space and time is saved by representing the cards as numbers
        # The number/10 is the value of the card and number%10 is the suit
        d = np.concatenate((np.arange(11, 141, 10, dtype=np.uint8), np.arange(11, 141, 10, dtype=np.uint8),
                            np.arange(12, 142, 10, dtype=np.uint8), np.arange(12, 142, 10, dtype=np.uint8),
                            np.arange(13, 143, 10, dtype=np.uint8), np.arange(13, 143, 10, dtype=np.uint8),
                            np.arange(14, 144, 10, dtype=np.uint8), np.arange(14, 144, 10, dtype=np.uint8)))

        # Shuffles the initialized deck, which would be in order otherwise
        np.random.shuffle(d)
        self.deck = d
        return d

    # Draws a card from the given deck d and puts it in the hand of the given player at index l
    # Returns the card drawn.
    def draw_card(self, p: Player, l: int) -> np.array:
        p.hand[l] = self.deck[0]
        self.deck = np.delete(self.deck, 0)

        if len(self.deck) <= 0:
            self.make_new_deck()
        return p.hand[l]

    # noinspection PyPep8Naming,SpellCheckingInspection
    def get_heuristic(self, botToken: int):
        tboard = self.tboard
        score = 0
        #scoreChart = [0, 0, 1, 3, 20, 1000, 1000, 1000, 1000, 1000]
        #jackValue = 15
        # Score values adjusted for use with hybrid
        scoreChart = [0, 0, 100, 300, 2000, 100000, 100000, 100000, 100000, 100000]
        jackValue = 1500

        def inBounds(xx, yy):
            if xx < 0 or xx > 9 or yy < 0 or yy > 9:
                return False
            return True

        def score_dir(token: int, xx: int, yy: int, xDir: int, yDir: int):

            x = xx
            y = yy

            if not (tboard[y][x] == token or tboard[y][x] == 0 or tboard[y][x] == 11):
                return 0
            streak = 0
            counter = 0

            while counter < 5:
                if not inBounds(x, y):
                    return 0
                elif tboard[y][x] == 11 or tboard[y][x] == token:
                    streak += 1
                    counter += 1
                    x += xDir
                    y += yDir
                elif tboard[y][x] == 0:
                    counter += 1
                    x += xDir
                    y += yDir
                else:
                    return 0
            return scoreChart[streak]


        # for every Player in game curToken = player token
        # then if curToken = botToken add! else substract 1/(number of players)
        for player in self.players:
            curToken = player.token
            curScore = 0
            for y in range(0, 10):
                for x in range(0, 10):
                    if tboard[y][x] == curToken or tboard[y][x] == 11 or tboard[y][x] == 0:
                        curScore += score_dir(curToken, x, y, 1, 0)
                        curScore += score_dir(curToken, x, y, 0, 1)
                        curScore += score_dir(curToken, x, y, 1, 1)
                        curScore += score_dir(curToken, x, y, 1, -1)

            if curToken == botToken:
                score += curScore
            else:
                score -= curScore*(1/(len(self.players)-1))
        for card in self.players[(self.current - 1) % len(self.players)].hand:
            if card == 111 or card == 112 or card == 113 or card == 114:
                score += jackValue

        return score


    # Checks if the recently placed move "last" resulted in a sequence
    def check_sequence(self) -> bool:
        if self.last_move is not None:
            lx = self.last_move[0][0]
            ly = self.last_move[0][1]
            mt = self.players[self.current].token

            # Checks for a horizontal sequence
            count: int = 1
            xx = lx - 1
            yy = ly
            while xx >= 0 and (self.tboard[xx][yy] == mt or self.tboard[xx][yy] == 11):
                count += 1
                xx -= 1
            xx = lx + 1
            while xx < 10 and (self.tboard[xx][yy] == mt or self.tboard[xx][yy] == 11):
                count += 1
                xx += 1
            if count > 4:
                return True

            # Checks for a vertical sequence
            count = 1
            xx = lx
            yy = ly - 1
            while yy >= 0 and (self.tboard[xx][yy] == mt or self.tboard[xx][yy] == 11):
                count += 1
                yy -= 1
            yy = ly + 1
            while yy < 10 and (self.tboard[xx][yy] == mt or self.tboard[xx][yy] == 11):
                count += 1
                yy += 1
            if count > 4:
                return True

            # Checks for left/right diagonal
            count = 1
            xx = lx - 1
            yy = ly - 1
            while (xx >= 0 and yy >= 0) and (self.tboard[xx][yy] == mt or self.tboard[xx][yy] == 11):
                count += 1
                xx -= 1
                yy -= 1
            xx = lx + 1
            yy = ly + 1
            while (xx < 10 and yy < 10) and (self.tboard[xx][yy] == mt or self.tboard[xx][yy] == 11):
                count += 1
                xx += 1
                yy += 1
            if count > 4:
                return True

            # Checks for right/left diagonal
            count = 1
            xx = lx + 1
            yy = ly - 1
            while (xx < 10 and yy >= 0) and (self.tboard[xx][yy] == mt or self.tboard[xx][yy] == 11):
                count += 1
                xx += 1
                yy -= 1
            xx = lx - 1
            yy = ly + 1
            while (xx >= 0 and yy < 10) and (self.tboard[xx][yy] == mt or self.tboard[xx][yy] == 11):
                count += 1
                xx -= 1
                yy += 1
            return count > 4
        return False

    # Gets a set of legal moves given a card
    def get_legal_moves(self, c: int, l: int) -> Set:
        legals: Set = set()
        # If the card is not a Jack, then we simply add the locations that are unclaimed from card_to_cords
        if c // 10 != 11:
            c_locations = self.card_to_cords[c]
            for location in c_locations:
                if self.tboard[location[0]][location[1]] == 0:
                    legals.add((location, l))
        else:
            # If the card is a Jack, then if it's black suit it can go on any unclaimed spot
            if c % 10 == 1 or c % 10 == 2:
                for xx in range(10):
                    for yy in range(10):
                        if self.tboard[xx][yy] == 0: # and self.cords_to_card[(xx, yy)] not in self.players[self.current].hand:
                            legals.add(((xx, yy), l))
            # If the Jack isn't black suit, then it can unclaim a claimed spot
            else:
                for xx in range(10):
                    for yy in range(10):
                        if self.tboard[xx][yy] != 0 and self.tboard[xx][yy] != 11:
                            legals.add(((xx, yy), l))
        return legals

    # Gets all legal moves for every card in a hand
    # Returns a list of each set per card
    def get_legal_moves_hand(self, p: Player):
        lm: Set = set()
        dupes: Set = set()
        replacement = None
        # Get all legal moves for cards in hand
        for cc in range(len(p.hand)):
            c: int = p.hand[cc]

            # If the card has already been processed (or for wildcards, its sibling) then skip it, there are no new legals
            if c in dupes:
                continue
            elif c // 10 == 11:
                if c == 111 and 112 in dupes:
                    continue
                elif c == 112 and 111 in dupes:
                    continue
                elif c == 113 and 114 in dupes:
                    continue
                elif c == 114 and 113 in dupes:
                    continue
            dupes.add(c)
            moves = self.get_legal_moves(c, cc)

            # If the card has no legal moves and it isn't a Jack, then it's a dead card and can be exchanged for a new card.
            if len(moves) == 0 and c != 113 and c != 114:

                # Mark this card as a dead card to be exchanged if there is not another already
                if replacement is None:
                    replacement = cc
            # Adds the found legal moves to the list at the index corresponding to card in hand
            lm = lm.union(moves)

        # If a dead card was marked to be replaced
        if replacement is not None:

            # Draws a replacement card at the marked index
            self.draw_card(p, replacement)
            lm = lm.union(self.get_legal_moves(p.hand[replacement], replacement))

        return lm

    # Place player token on the selected move if the spot is unclaimed.
    # If the spot is claimed, then the player played a red jack and it can be removed
    # The player discards the card they just played and draws card from the deck
    # Sets the last move to this one
    def put(self, move: Tuple[Tuple[int, int], int]):
        self.draw_count = 0
        self.tboard[move[0][0]][move[0][1]] = self.players[self.current].token \
            if self.tboard[move[0][0]][move[0][1]] == 0 else 0
        played = self.players[self.current].hand[move[1]]
        skip = played == 113 or played == 114
        self.draw_card(self.players[self.current], move[1])
        self.last_move = move
        self.last_player = self.current
        if (not skip) and self.check_sequence():
            #ConsoleUI.pretty_print(self.tboard, self.cords_to_card)
            self.victor = self.current
            #ConsoleUI.show_good(str(self.current))
            self.change_player()
            #ConsoleUI.show_debug(str((self.current - 1) % len(self.players)))
            #ConsoleUI.show_debug(str(self.last_player))
        else:
            self.change_player()

    def change_player(self) -> Player:
        self.turn_count += 1
        self.current = (self.current + 1) % len(self.players)
        return self.players[self.current]

    def copy(self):
        ab: Board = Board(False)
        ab.tboard = copy.deepcopy(self.tboard)
        ab.deck = copy.deepcopy(self.deck)
        ab.players = copy.deepcopy(self.players)
        ab.last_move = copy.deepcopy(self.last_move)
        ab.current = self.current
        ab.turn_count = self.turn_count
        ab.victor = self.victor
        ab.last_player = self.last_player
        ab.draw_count = self.draw_count
        return ab






