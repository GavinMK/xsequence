# README #

Sequence AI Members:
	Gavin Kremer,
	Clara Nguyen,
	Alessandro Cois
	

-- Report -- The report is named "SequenceReport.pdf" and is also on the bitbucket repository download page. 


-- Code -- Most of the files are for the game itself. Functions of interest for our actual AI are listed below:


	"MCTS" function in main.py: The Monte Carlo Tree Search implementation. Uses several other functions that should be easily traceable.
	
	"HAI" function in main.py: The Heuristic AI implementation.
	
	"get_heuistic" in board.py: The heuristic function used in HAI
	
	"HATS" function in main.py: The hybrid between MCTS and HAI. 
	
	bitbucket link: https://bitbucket.org/GavinMK/xsequence/src/master/
	
	You need numpy and typing for this to work. Numpy was used in the board and typing was used throughout the code. Make sure you run with python3


-- Data -- Everything besides the functions listed above and those that they call is for the game itself
