from typing import Dict, List, Tuple, Set
from ui import ConsoleUI
from player import Player
import time
from board import Board
import random
import math
import numpy as np


# Method to obtain a move decision from a human player. Just uses the UI to make them
# Choose a legal position.
def player_turn(b: Board) -> Board:
    cp: Player = b.players[b.current]
    move: Tuple[int, int] = None

    # The card's index in hand
    cardi: int
    while move is None:

        # Show current player's hand
        ConsoleUI.pretty_print_hand(cp.name, cp.hand)

        # Gets card choice from user
        cardi = ConsoleUI.get_card(cp.hand)

        # Reprints the board with the legal moves highlighted for player visibility
        ConsoleUI.pretty_print_legals(b.tboard, b.cords_to_card, b.get_legal_moves(cp.hand[cardi], cardi))
        ConsoleUI.pretty_print_hand(cp.name, cp.hand)

        # Have player enter a move that is in the legal moves set for the chosen card
        # Returns None if player changes their mind
        move = ConsoleUI.get_move(b.get_legal_moves(cp.hand[cardi], cardi))

    b.put((move, cardi))

    return b


# A node that contains metadata for our tree.
class MCTSNode:
    # The board associated with this node
    state: Board = None

    # The number of times this node was visited.
    visits: float = None

    # Number of times this node has led to a victory
    wins: float = None

    # The parent of this node in the tree
    parent: 'MCTSNode' = None

    # The children of this node in the tree
    children = None

    def __init__(self, state: Board):
        self.state = state
        self.visits = 0
        self.wins = 0
        self.children = []

# Implementation of the "Upper Confidence Bound for Trees" (UCT) formula
# Basically just finds the best node to expand in the tree
def UCT_score(n: MCTSNode) -> float:
    if n.visits == 0:
        return 9999
    else:
        return (n.wins/n.visits) + (math.sqrt(2 * math.log(n.parent.visits) / n.visits))


# Returns the node in the subtree from the given root with the highest UCT score to be expanded
def MCTS_selection(root: MCTSNode) -> MCTSNode:
    node_iter: MCTSNode = root
    while len(node_iter.children) != 0:
        node_iter = max(node_iter.children, key=lambda c: UCT_score(c))
    return node_iter


# Expands a node by creating child nodes from all possible board states
# Using the given MCTSNode as the root
def MCTS_expansion(n: MCTSNode):
    lm = n.state.get_legal_moves_hand(n.state.players[n.state.current])
    for move in lm:
        ab: Board = n.state.copy()
        ab.put(move)
        new_node: MCTSNode = MCTSNode(ab)
        n.children.append(new_node)
        new_node.parent = n


# Simulates a game playout by selecting nodes until a terminal state is reached.
# Uses the given MCTSNode as the starting point for the simulation
# Returns the player that won the game
def MCTS_simulation(n: MCTSNode) -> int:
    ab: Board = n.state.copy()
    #np.random.shuffle(ab.deck)
    while ab.victor is None:
        lm: Set = ab.get_legal_moves_hand(ab.players[ab.current])
        if len(lm) == 0:
            ab.draw_count += 1
            if ab.draw_count >= len(ab.players):
                return -1
        else:
            rng = random.choice(tuple(lm))
            ab.put(rng)
    return ab.victor


# Back propagates our metadata. Every node in the path
def MCTS_backpropagate(n: MCTSNode, p: int):
    node_iter: MCTSNode = n
    while node_iter is not None:
        if p == -1:
            node_iter.wins += 5
        elif node_iter.state.last_player == p:
            node_iter.wins += 10
        node_iter.visits += 1
        node_iter = node_iter.parent


def MCTS(b: Board) -> Board:

    ConsoleUI.show_debug("Planning my next move...")
    #ConsoleUI.pretty_print_hand(b.players[b.current].name, b.players[b.current].hand)

    # The amount of time an AI is allowed to spend making a decision per turn
    TIME_LIMIT = 8
    start_time = time.process_time()

    # Root of the tree to be constructed
    # Uses the parameter as the initial state
    root: MCTSNode = MCTSNode(b)

    # While the time limit has not been met
    while time.process_time() - start_time < TIME_LIMIT:

        # print(time.process_time() - start_time)

        # Selects a node using MCTS_selection
        selected_node: MCTSNode = MCTS_selection(root)

        # If the selected node is not a game ending move
        if selected_node.state.victor is None:

            # Creates all children nodes using MCTS_expansion
            MCTS_expansion(selected_node)

            # Randomly chooses one of the selected node's children to be explored
            exploration_node: MCTSNode = random.choice(selected_node.children)

            # Simulates a game playout from the exploration node and updates the tree metadata accordingly
            MCTS_backpropagate(exploration_node, MCTS_simulation(exploration_node))
        else:
            MCTS_backpropagate(selected_node, MCTS_simulation(selected_node))

    ConsoleUI.show_debug("After computing " + str(root.visits) + " possibilites, I have determined that THIS is the best move, human.")
    return max(root.children, key=lambda c: c.wins).state


def HAI(b: Board) -> Board:

    # Get the current Player
    cp: Player = b.players[b.current]

    ConsoleUI.show_debug("Planning my next move...")
    #ConsoleUI.pretty_print_hand(b.players[b.current].name, b.players[b.current].hand)

    # Create a list that has [move, cardi, heursticScore]
    possibleMoves = []

    # initialize possibleMoves
    cardi = -1
    for each in cp.hand:
        cardi += 1
        for move in b.get_legal_moves(cp.hand[cardi], cardi):
            possibleMoves.append([move, 0])


    # Get the heuristics for possibleMoves
    for pMove in possibleMoves:
        testBoard = b.copy()
        testBoard.put(pMove[0])
        pMove[1] = testBoard.get_heuristic(cp.token)

    # Play the move with the highest heuristic
    best = possibleMoves[0]
    for pMove in possibleMoves:
        if pMove[1] > best[1]:
            best = pMove

    b.put(best[0])

    return b

def HATS(b: Board) -> Board:

    ConsoleUI.show_debug("Planning my next move...")

    # The amount of time an AI is allowed to spend making a decision per turn
    TIME_LIMIT = 8
    start_time = time.process_time()
    me = b.players[b.current].token

    # Root of the tree to be constructed
    # Uses the parameter as the initial state
    root: MCTSNode = MCTSNode(b)

    # While the time limit has not been met
    while time.process_time() - start_time < TIME_LIMIT:

        # print(time.process_time() - start_time)

        # Selects a node using MCTS_selection
        selected_node: MCTSNode = MCTS_selection(root)

        # If the selected node is not a game ending move
        if selected_node.state.victor is None:

            # Creates all children nodes using MCTS_expansion
            MCTS_expansion(selected_node)

            # Randomly chooses one of the selected node's children to be explored
            exploration_node: MCTSNode = random.choice(selected_node.children)

            # Simulates a game playout from the exploration node and updates the tree metadata accordingly
            MCTS_backpropagate(exploration_node, MCTS_simulation(exploration_node))
        else:
            MCTS_backpropagate(selected_node, MCTS_simulation(selected_node))

    ConsoleUI.show_debug("After computing " + str(root.visits) + " possibilites, I have determined that THIS is the best move, human.")
    best = None
    oh = root.state.get_heuristic(me)
    for child in root.children:
        h = child.state.get_heuristic(me)
        dh = h - oh
        if best is None or dh + child.wins > best[1]:
            best = (child, dh + child.wins)
    return best[0].state


# Set this to true to start every hand with each Jack. Useful for debugging.
all_jack: bool = False

# Set this to true to run with multiple simulations
use_loop: bool = False

mb: Board = Board(True)

mb.players = ConsoleUI.get_players()

# The hand size of the players which is dependant on the amount in game
hand_size: int = 7 if len(mb.players) == 2 else 6 if len(mb.players) == 3 else 5
for player in mb.players:
    # Initializes the player's hand at this point since np.array must be of static size.
    player.create_hand(hand_size)
for i in range(hand_size):
    for player in mb.players:
        mb.draw_card(player, i)


# The main game function, which finds a winner with the given deck d, token layout tb, and players list ps
# Returns the player that won the game
# This function will be called many times with new arguments by our AI who will compare the return value to itself
# to measure the effectiveness of its moves.
def virtual_game(b: Board) -> int:
    winner: int = None

    # Until a winner is found repeat the following
    while winner is None:

        # The player whose turn it currently is
        current_player: Player = b.players[b.current]

        # The player's hand. Overwritten to all jacks if all_jack is true
        if all_jack:
            current_player.hand = [111, 112, 111, 112, 111, 112]

        # Show the board
        ConsoleUI.pretty_print_lm(b.tboard, b.cords_to_card, b.last_move[0]) if b.last_move is not None else \
            ConsoleUI.pretty_print(b.tboard, b.cords_to_card)
        if current_player.is_ai == 1:
            b = MCTS(b)
        elif current_player.is_ai == 2:
            b = HAI(b)
        elif current_player.is_ai == 3:
            b = HATS(b)
        else:
            b = player_turn(b)

        # If the move resulted in a sequence, then the current player is the winner!
        if b.victor is not None:
            ConsoleUI.pretty_print(b.tboard, b.cords_to_card)
            winner = b.victor
    if winner is None:
        ConsoleUI.show_good("The game ended in a draw!")
    else:
        ConsoleUI.show_good("The winner is " + mb.players[winner].name + "!")
    return winner


# This function is purely for testing benchmarks and is in no way robust.
def loop_game(b: Board) -> float:
    p1: int = 0
    p2: int = 0
    GAMECOUNT: int = 100
    game: int = 0

    # Until a winner is found repeat the following
    while game < GAMECOUNT:
        print("GAME: ",game,"############################################################################################")
        while b.victor is None:

            # The player whose turn it currently is
            current_player: Player = b.players[b.current]

            if current_player.is_ai == 1:
                b = MCTS(b)
            elif current_player.is_ai == 2:
                b = HAI(b)
            elif current_player.is_ai == 3:
                b = HATS(b)
            else:
                b = player_turn(b)

            # If the move resulted in a sequence, then the current player is the winner!
            if b.victor is not None:
                ConsoleUI.pretty_print(b.tboard, b.cords_to_card)
                winner = b.victor
                if winner == 0:
                    p1 += 1
                elif winner == 1:
                    p2 += 1
        game += 1
        print(b.turn_count)
        print("Current Ratio is:",p1/game)
        nb = Board(True)
        nb.players = b.players
        for player in nb.players:
            # Initializes the player's hand at this point since np.array must be of static size.
            player.create_hand(hand_size)
        for i in range(hand_size):
            for player in nb.players:
                nb.draw_card(player, i)
        b = nb

    ConsoleUI.show_good("p1 win ratio is: " + str(p1/GAMECOUNT) + "!")
    return p1/GAMECOUNT


ConsoleUI.pretty_print(mb.tboard, mb.cords_to_card)

if use_loop:
    loop_game(mb)
else:
    virtual_game(mb)







